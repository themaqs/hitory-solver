"""
Config module
"""


class GlobalConf:
    """
    global data class for storing configs
    """
    __instance = None
    __col_dlm = ' '
    __line_dlm = '\n'
    __file_addr = './files'

    @staticmethod
    def instance():
        """
        singleton pattern
        :return: GlobalConf
        """
        if GlobalConf.__instance is None:
            GlobalConf.__instance = GlobalConf()
        return GlobalConf.__instance

    @property
    def col_dlm(self) -> str:
        """
        column delimiter getter
        :return: str
        """
        return self.__col_dlm

    @property
    def line_dlm(self) -> str:
        """
        line delimiter getter
        :return: str
        """
        return self.__line_dlm

    def file_addr(self) -> str:
        """
        file address getter
        :return: str
        """
        return self.__file_addr
