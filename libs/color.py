"""
Color enumeration module
"""
import enum


class Color(enum.Enum):
    """
    enumeration class
    """
    White = 1
    Black = -1
