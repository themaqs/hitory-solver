"""
Cell module
"""
from libs.color import Color


class Cell:
    """
    cell class for storing each cell data on map
    """
    __x: int
    __y: int
    __value: int
    __color: Color

    def __init__(self, x_dir: int, y_dir: int, value: int, color=Color.White):
        """
        constructor
        :param x_dir: int
        :param y_dir: int
        :param value: int
        :param color: Color
        """
        self.x_dir = x_dir
        self.y_dir = y_dir
        self.value = value
        self.color = color

    @staticmethod
    def ensure_positive(param: int):
        """
        ensuring positive and zero values
        :param param: int
        """
        if param < 0:
            raise Exception("The cell parameters ({0})must be greater than zero!".format(param))

    @property
    def x_dir(self) -> int:
        """
        x getter
        :return: int
        """
        return self.__x

    @x_dir.setter
    def x_dir(self, x_dir: int):
        """
        x setter
        :param x_dir: int
        """
        Cell.ensure_positive(x_dir)
        self.__x = x_dir

    @property
    def y_dir(self) -> int:
        """
        y getter
        :return: int
        """
        return self.__y

    @y_dir.setter
    def y_dir(self, y_dir: int):
        """
        y setter
        :param y_dir: int
        """
        Cell.ensure_positive(y_dir)
        self.__y = y_dir

    @property
    def value(self) -> int:
        """
        value getter
        :return: int
        """
        return self.__value

    @value.setter
    def value(self, value: int):
        """
        y setter
        :param y: int
        """
        Cell.ensure_positive(value)
        self.__y = value

    @property
    def color(self) -> Color:
        """
        color getter
        :return: Color
        """
        return self.__color

    @color.setter
    def color(self, color: Color):
        """
        color setter
        :param color: Color
        """
        self.__color = color
